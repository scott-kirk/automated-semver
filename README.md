# Automated-semver

Automated-semver is a tiny standalone tool to help manage release versions for your software.
It will analyze your git history and automatically determine the next logical version bump
based off of the labels on your commit messages. Documentation for the SemVer versioning scheme
can be found [here.](https://semver.org/) A blog post about this project is
[here.](https://scottkirk.dev/blog/gitlab-releases/gitlab-releases/)

## Usage

The easiest way to hook this into your project is to grab the
[latest released image](https://gitlab.com/scott-kirk/automated-semver/-/releases) from
[docker hub](https://hub.docker.com/r/skirkr/automated-semver) and then
run the container with your repo mounted in. If your project is in GitLab you can also take a
look at the [pipeline for this project](https://gitlab.com/scott-kirk/automated-semver/-/blob/master/.gitlab-ci.yml),
which uses the tool to version itself.

The program determines the repo location through either supplying the environment variable `REPO_PATH`
or by calling the tool with the repo path as the first argument. The examples of the two methods
are below:

```
REPO_PATH=/home/projects/my_repo automated-semver
```
```
automated-semver /home/projects/my_repo
```

An example with docker that works when in the repo:
```
docker run -v $(pwd):/home/repo skirkr/automated-semver:1.0.1 /home/repo
```

Once the tool is integrated into your project you'll need to start following a format for
your commit messages in order to help the tool parse out the impact of the commit. The tool
will extract the final word of the commit message and map it to a corresponding version bump.
If your commit does not affect any released code, then the keyword `infra` can be used and
the tool will not factor that commit in when determining the next version.

If your commit message does not end with `major`, `minor`, `patch`, or `infra`, the tool will
default to a `patch` version bump.

## Contributing

The project is written in Rust and is confined to a single source file. While this tool
works enough for my own projects and conventions, feel free to make a merge request for
any enhancements you need for your own workflow.
