use core::fmt;
use git2;
use git2::{DescribeOptions, ObjectType, Repository};
use std::env;
use std::error::Error;
use std::fmt::Formatter;
use std::process::exit;

#[derive(Clone, Copy, PartialEq)]
struct SemVer {
    major: i32,
    minor: i32,
    patch: i32,
}

impl SemVer {
    fn create_semver(tag: String) -> SemVer {
        let mut tag: Vec<&str> = tag.split('.').collect();
        let patch: i32 = tag.pop().unwrap().parse().unwrap();
        let minor: i32 = tag.pop().unwrap().parse().unwrap();
        let major: i32 = tag.pop().unwrap().parse().unwrap();
        SemVer {
            major,
            minor,
            patch,
        }
    }

    fn bump_ver(&mut self, bump_type: VerBump) -> &mut SemVer {
        match bump_type {
            VerBump::MAJOR => {
                self.major += 1;
                self.minor = 0;
                self.patch = 0;
            }
            VerBump::MINOR => {
                self.minor += 1;
                self.patch = 0;
            }
            VerBump::PATCH => self.patch += 1,
            VerBump::INFRA => {}
        }
        self
    }
}

impl fmt::Display for SemVer {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}

#[derive(PartialEq)]
enum VerBump {
    MAJOR,
    MINOR,
    PATCH,
    INFRA,
}

impl VerBump {
    fn from_label(label: &str) -> VerBump {
        match label {
            "major" => VerBump::MAJOR,
            "minor" => VerBump::MINOR,
            "patch" => VerBump::PATCH,
            "infra" => VerBump::INFRA,
            _ => VerBump::PATCH,
        }
    }

    fn label_length() -> usize {
        5
    }

    fn max(a: VerBump, b: VerBump) -> VerBump {
        if a == VerBump::MAJOR || b == VerBump::MAJOR {
            VerBump::MAJOR
        } else if a == VerBump::MINOR || b == VerBump::MINOR {
            VerBump::MINOR
        } else if a == VerBump::PATCH || b == VerBump::PATCH {
            VerBump::PATCH
        } else {
            VerBump::INFRA
        }
    }
}

const REPO_ENV_VAR: &str = "REPO_PATH";
const REF_PREFIX: &str = "refs/tags/";

fn main() -> Result<(), Box<dyn Error>> {
    let repo_path = match env::var(REPO_ENV_VAR) {
        Ok(repo_location) => repo_location,
        Err(_) => {
            let mut repo_location: Vec<String> = env::args().collect();
            if repo_location.len() == 1 {
                panic!("{} not set and no repo given as argument", REPO_ENV_VAR)
            }
            String::from(repo_location.pop().unwrap())
        }
    };
    let repo = Repository::open(repo_path).unwrap();

    let mut revwalk = repo.revwalk().unwrap();
    revwalk.push_head().unwrap();
    let revwalk = revwalk;

    let latest_ref = get_latest_ref(&repo);
    if !latest_ref.contains('-') {
        println!(
            "The latest commit is already tagged with {}, aborting release",
            latest_ref
        );
        exit(42);
    }
    let previous_ver = get_semver(latest_ref);

    let last_tagged_commit_id = repo
        .find_reference((String::from(REF_PREFIX) + &previous_ver.to_string()).as_str())
        .unwrap()
        .peel(ObjectType::Commit)
        .unwrap()
        .id();
    let mut highest_bump = VerBump::INFRA;
    for object_id in revwalk {
        let object_id = object_id.unwrap();
        if object_id == last_tagged_commit_id {
            break;
        }
        let commit = repo.find_commit(object_id).unwrap();
        if commit.parent_count() > 1 {
            // ignore merge commits
            continue
        }
        let commit_message = commit.message().unwrap().trim().to_lowercase();
        let commit_suffix = &commit_message[commit_message.len() - VerBump::label_length()..];
        let commit_ver_bump = VerBump::from_label(commit_suffix);
        highest_bump = VerBump::max(highest_bump, commit_ver_bump);
    }
    let next_ver: SemVer = previous_ver.clone().bump_ver(highest_bump).to_owned();
    if next_ver == previous_ver {
        println!("No version bump required as no commit was labeled as a production code change");
        exit(42);
    }
    println!("{}", next_ver);
    Ok(())
}

fn get_latest_ref(repo: &Repository) -> String {
    let description = repo
        .describe(&DescribeOptions::new().describe_tags())
        .expect("No tag was found, make sure the current branch contains a tagged commit");
    description.format(None).unwrap()
}

fn get_semver(description: String) -> SemVer {
    let mut description: Vec<&str> = description.split('-').collect();
    // a described tag will be in the format of '{tag}-{commits_since_tag}-{sha_of_tag}'
    // the first pop after splitting will get rid of the sha of the tag
    // and the second pop will get rid of the commits since the tag
    description.pop().unwrap();
    description.pop().unwrap();
    let tag = description.pop().unwrap().to_string();
    SemVer::create_semver(tag)
}
