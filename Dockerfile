# ------------------------------------------------------------------------------
# Build Stage
# ------------------------------------------------------------------------------

FROM ekidd/rust-musl-builder:stable as build

RUN USER=root cargo new --bin automated-semver
WORKDIR ./automated-semver

COPY Cargo.lock Cargo.lock
COPY Cargo.toml Cargo.toml

RUN cargo build --release
RUN rm src/*.rs && rm ./target/x86_64-unknown-linux-musl/release/deps/automated_semver*

COPY ./src ./src

RUN cargo build --release

# ------------------------------------------------------------------------------
# Final Stage
# ------------------------------------------------------------------------------

FROM alpine:latest

RUN addgroup -g 1000 automator && adduser -D -s /bin/sh -u 1000 -G automator automator

COPY --from=build /home/rust/src/automated-semver/target/x86_64-unknown-linux-musl/release/automated-semver /bin/automated-semver

RUN chown automator:automator /bin/automated-semver

USER automator

ENTRYPOINT [ "automated-semver" ]
